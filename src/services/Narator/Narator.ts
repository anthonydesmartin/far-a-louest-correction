import { CanDialog } from './INarator';
import { CanSpeak } from './actors/IActor';

export class Narator implements CanDialog {
	private actor: CanSpeak;
	constructor(actor: CanSpeak) {
		this.actor = actor;
	}

	async dialog(text: string) {
		await this.actor.speak(text);
	}
}
