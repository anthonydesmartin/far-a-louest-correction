import { PrismaClient } from '@prisma/client';
import { IsConnectable } from './IDb';

export class PrismaConnector implements IsConnectable {
	private client: PrismaClient;
	public constructor() {
		this.client = new PrismaClient();
	}

	connect(): unknown {
		return this.client;
	}
}
