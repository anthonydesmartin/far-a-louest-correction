import { IsConnectable } from '../../services/db/IDb';
import { PrismaConnector } from '../../services/db/PrismaConnector';
import { generateId } from '../../services/idGeneration';
import { IPlayer } from '../player/IPlayer';
import { PlayerModel } from '../player/Player';
import { SceneModel } from '../scene/Scene';
import { ISession } from '../session/ISession';
import { SessionModel } from '../session/Session';
import { CanCreatePlayerModel, CanCreateSceneModel, CanCreateSessionModel } from './IModelFactory';

export class SqlModelFactory
	implements CanCreatePlayerModel, CanCreateSceneModel, CanCreateSessionModel
{
	private db: IsConnectable;
	constructor() {
		this.db = new PrismaConnector();
	}

	createPlayerModel(): IPlayer {
		return new PlayerModel(this.db);
	}

	createSessionModel(): ISession {
		return new SessionModel(this.db, generateId);
	}

	createSceneModel() {
		return new SceneModel(this.db);
	}
}
