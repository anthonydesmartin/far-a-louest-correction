import { CanGetDefaultScene, CanGetScene } from './IScene';
import { IsConnectable } from '../../services/db/IDb';
import { MongooseModel } from '../../services/db/MongooseConnector';

export class SceneModelMongo implements CanGetDefaultScene, CanGetScene {
	private db: MongooseModel;

	constructor(db: IsConnectable) {
		this.db = db.connect() as MongooseModel;
	}

	async getDefaultScene() {
		return this.getScene('default');
	}

	async getScene(scene: string) {
		const sceneFound = await this.db.Scene.findOne({
			name: scene,
		});

		if (!sceneFound) return null;

		const sceneObject = sceneFound.toObject();

		return {
			...sceneObject,
			id: sceneObject.name,
			actions: sceneObject.actions.map(
				(action: { type: string; link: string; content: string }) => ({
					...action,
					link: {
						id: action.link,
						type: action.type,
					},
				})
			),
		};
	}
}
