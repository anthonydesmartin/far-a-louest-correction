import { CompleteScene } from '../../services/types';

export type IScene = CanGetDefaultScene & CanGetScene;

export interface CanGetDefaultScene {
	getDefaultScene(): Promise<CompleteScene | null>;
}

export interface CanGetScene {
	getScene(scene: string): Promise<CompleteScene | null>;
}
