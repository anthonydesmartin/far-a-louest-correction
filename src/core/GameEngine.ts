import { select } from '@inquirer/prompts';
import { CanCreateSceneModel } from '../models/factory/IModelFactory';
import { CanDialog } from '../services/Narator/INarator';
import { GameBoostrapper } from './GameBoostrapper';
import { GameInfo } from './GameInfo';
import { GameSaver } from './GameSaver';

export class GameEngine {
	private gameState: GameInfo | undefined;
	constructor(
		private bootstraper: GameBoostrapper,
		private modelFactory: CanCreateSceneModel,
		private narators: { [key: string]: CanDialog },
		private gameSaver: GameSaver
	) {}

	async setup() {
		this.gameState = await this.bootstraper.init();
	}

	async start() {
		if (!this.gameState) throw new Error('No game state not initialized.');
		while (!this.gameState.hasEnded()) {
			await this.loop();

			if (this.gameState.hasEnded()) {
				this.gameSaver.save(this.gameState);
				setTimeout(() => process.exit(0), 1000); // J'ai du le rajouter, sinon je ne sais pas pourquoi il ne veut pas quitter le jeu
			}
		}
	}

	private async loop(): Promise<void> {
		console.clear();
		if (!this.gameState) {
			throw new Error('No game state initialized.');
		}

		const scene = this.gameState.getNextScript();
		if (!scene) {
			throw new Error('No scene found. Session might be corrupted.');
		}

		for (const dialog of scene.dialogs) {
			await this.narators[dialog.speaker].dialog(dialog.content);
		}

		const choices = scene.actions.map((item) => ({
			name: item.content,
			value: item.link.id,
		}));

		const answer = await select(
			{
				message: 'Quelle action souhaitez-vous effectuer ?',
				choices,
			},
			{
				clearPromptOnDone: true,
			}
		);

		const nextScene = await this.modelFactory.createSceneModel().getScene(answer);
		if (!nextScene) throw new Error('No next scene found. Session might be corrupted.');

		await this.gameState.setNextElementScript(nextScene);
	}
}
