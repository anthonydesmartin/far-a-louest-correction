import { CanCreateSessionModel } from '../models/factory/IModelFactory';
import { CanUpdateSessionProgress } from '../models/session/ISession';
import { GameInfo } from './GameInfo';

export class GameSaver {
	private sessionModel: CanUpdateSessionProgress;

	constructor(modelFactory: CanCreateSessionModel) {
		this.sessionModel = modelFactory.createSessionModel();
	}

	save(gameState: GameInfo) {
		const session = gameState.getSession();
		const nextScene = gameState.getNextScript();
		if (!session || !nextScene) throw new Error('There is no session or scene to save to save');

		return this.sessionModel.updateSessionProgress(session, nextScene.id);
	}
}
